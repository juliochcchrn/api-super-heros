
let heroesData = [];
// Créer un ensemble pour les noms de héros
let heroesName = new Set();
// Créer un ensemble pour les éditeurs de héros
let heroesPublisher = new Set();

// Fonction pour récuperer les données JSON depuis un fichier local
function getData() {
    //affiche le loader
    document.getElementById('loader').style.display = 'block';

    //Charger le fichier JSON local
    fetch(`superHeros.json`)
        //fonction  qui convertit la réponse http en un objet js
        .then(response => response.json())
        // Si données récupérées, stocker les infos dans une variable
        .then(data => {
            heroesData = data;
            //Appel de la fonction displayData
            displayData();
            
            // Parcourir les données pour récupérer les auteurs et les catégories distincts, La méthode add() vérifie automatiquement 
            // si l'élément ajouté est déja présent. Si déja présent, ne l'ajoute pas une seconde fois 
            heroesData.supersHeros.forEach(hero => {
                heroesName.add(hero.name);
                heroesPublisher.add(hero.biography.publisher);   
            });
            //Appel de la fonction fillDropDown
            fillDropdowns();
            // Masquer le loader lorsque les données sont chargées
            document.getElementById('loader').style.display = 'none';
        })
        //Si erreur dans la récuperation, afficher un message d'erreur dans la console
        .catch(error => {
            console.error('Erreur:', error);
            // Masquer le loader en cas d'erreur
            document.getElementById('loader').style.display = 'none';
        });
}
// Définir la fonction displayFilteredHeroes en dehors de fillDropdowns
function displayFilteredHeroes(filteredHeroes) {
    const display = document.getElementById('display');
    // Utiliser la méthode map() pour créer une carte pour chaque super-héros filtré et les joindre en une chaîne
    display.innerHTML = filteredHeroes.map(hero => {
        return `
            <div class="col-md-3 col-sm-6 --bs-white-border-subtle" key=${hero.id}>
                <img class="card-img-top" src=${hero.images.md} alt="thumbnail" />
                <h3 class="card-body bg-danger text-center p-3">${hero.name}</h3>
            </div>`;
    }).join('');
}

// Fonction pour remplir les menus déroulants avec les héros et éditeurs distincts
function fillDropdowns() {
    let superHerosNameDrop = document.getElementById("superHerosName"); // L'élément HTML du menu déroulant pour les héros
    let superHerosPublisherDrop = document.getElementById("superHerosPublisher"); // L'élément HTML du menu déroulant pour les éditeurs

    // Remplir le menu déroulant avec les héros distincts
    heroesName.forEach(name => {
        // Condition qui vérifie qu'il y a bien un nom et non pas une valeur vide
        if (name !== "") {
            //Crée une nouvelle option dans le select superHeroName
            let option = document.createElement("option");
            option.value = name;
            option.textContent = name;
            superHerosNameDrop.appendChild(option);
        }
    });

    // Remplir le menu déroulant avec les éditeurs distincts
    const sortedPublishers = Array.from(heroesPublisher).sort(); //Nouveau tableau des éditeurs trié par ordre alphabetique
    // Condition qui vérifie qu'il y a bien un nom et non pas une valeur vide
    sortedPublishers.forEach(publisher => {
        if (publisher !== "") {
            //Crée une nouvelle option dans le select superHeroName
            let option = document.createElement("option");
            option.value = publisher;
            option.textContent = publisher;
            superHerosPublisherDrop.appendChild(option);
        }
    });

    // Ajouter l'écouteur d'événements sur superHerosPublisherDrop
    superHerosPublisherDrop.addEventListener("change", function() {
        const selectedPublisher = superHerosPublisherDrop.value; // Récupérer l'éditeur sélectionné
        
        //Si la selection est "all", affiche toutes les cards
        if (selectedPublisher === "all") {
            displayData(); // Afficher tous les héros
             // Activer le menu déroulant des éditeurs de héros
             document.getElementById('superHerosName').disabled = false;
        } else {
            // Filtrer les héros en fonction de l'éditeur sélectionné
            const filteredHeroes = heroesData.supersHeros.filter(hero => hero.biography.publisher === selectedPublisher);
            // Afficher les héros filtrés en utilisant la fonction displayFilteredHeroes
            displayFilteredHeroes(filteredHeroes);
             // Desactiver le menu déroulant des éditeurs de héros
            document.getElementById('superHerosName').disabled = true;
        }
    });
    superHerosNameDrop.addEventListener("change", function() {
        const selectedHero = superHerosNameDrop.value; // Récupérer l'héro sélectionné
        //Si la selection est "all", affiche toutes les cards
        if (selectedHero === "all") {
            displayData(); // Afficher tous les héros
            // Activer le menu déroulant des éditeurs de héros
            document.getElementById('superHerosPublisher').disabled = false;
        } else {
            // Desactiver le menu déroulant des éditeurs de héros
            document.getElementById('superHerosPublisher').disabled = true;
        }
    });
}

//Fonction pour afficher toutes les cards dans le conteneur
function displayData(heroes) {
    const display = document.getElementById('display');
    // Utilisation de la méthode map() pour créer une carte pour chaque super-héros et les joindre en une chaîne
    display.innerHTML = heroesData.supersHeros.map(hero => {
        return `
            <div class="col-md-3 col-sm-6 --bs-white-border-subtle" key=${hero.id}>
                <img class="card-img-top" src=${hero.images.md} alt="thumbnail" />
                <h3 class="card-body bg-danger text-center p-3">${hero.name}</h3>
            </div>`;
        // Joindre toutes les cartes en une seule chaîne de caractères
    }).join('');
}

const superHerosNameSelect = document.getElementById("superHerosName");
// Créer un evenement quand un héro est sélectionné dans SuperHerosName
superHerosNameSelect.addEventListener("change", function() {

    //récuperer le nom du héro sélectionné
    const selectedHeroName = superHerosNameSelect.value;

    //Rechercher le héro correspondant dans le json
    const selectedHero = heroesData.supersHeros.find(hero => hero.name === selectedHeroName);
    
    // Afficher les détails du héro dans la modal en appelant la fonction modalDisplayHero
    modalDisplayHero(selectedHero);
});

// Fonction pour afficher les détails du héros dans la modal quand on sélectionne son nom dans le select superHerosName
function modalDisplayHero(hero) {
    // Récupérer les éléments HTML de la modal
    const heroImage = document.getElementById("heroImage");
    const heroName = document.getElementById("heroName");
    const heroStats = document.getElementById("heroStats");

    // Vérifie que la liste des statistiques est vide avant d'ajouter de nouvelles statistiques afin d'éviter les doublons
    heroStats.innerHTML = '';

    // Mettre à jour le contenu de la modal avec l'image et le nom du héro
    heroImage.src = hero.images.md;
    heroName.textContent = hero.name;
    
    // Ajouter chaque statistique comme un élément li
    for (const [stat, value] of Object.entries(hero.powerstats)) {
        const statItem = document.createElement("li");
        statItem.textContent = `${stat}: ${value}`;
        heroStats.appendChild(statItem);
    }

    // Afficher la modal avec son code boostrap ( heroModal.show() == heroModal.style.display = '');
    const heroModal = new bootstrap.Modal(document.getElementById('heroModal'));
    heroModal.show();
}

// Appeler la fonction pour récupérer les données au chargement de la page
window.onload = getData;